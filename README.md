# Containerised `snakemake`

A container for `snakemake`

## Running

Run with

```sh
podman run --rm -it -v "${PWD}:/pwd:z" registry.gitlab.com/containers.ada/snakemake
```

## Persistence

Currently no persistence, maybe want to also include the `conda` environment directory as a volume for people who make heavy use of the `conda:` keyword.
